package com.hpe.srf.automation.tools.model;

public interface SecretContainer {
    
    void initialize(String secret);
}

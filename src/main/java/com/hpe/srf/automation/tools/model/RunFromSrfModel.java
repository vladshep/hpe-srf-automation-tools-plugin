// (c) Copyright 2012 Hewlett-Packard Development Company, L.P. 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package com.hpe.srf.automation.tools.model;

import org.kohsuke.stapler.DataBoundConstructor;

import java.util.Arrays;
import java.util.List;

public class RunFromSrfModel {

    public final static EnumDescription runModeLocal = new EnumDescription(
            "RUN_LOCAL", "Run locally");
    public final static EnumDescription runModePlannedHost = new EnumDescription(
            "RUN_PLANNED_HOST", "Run on planned host");
    public final static EnumDescription runModeRemote = new EnumDescription(
            "RUN_REMOTE", "Run remotely");
    public final static List<EnumDescription> runModes = Arrays.asList(
            runModeLocal, runModePlannedHost, runModeRemote);
    public final static int DEFAULT_TIMEOUT = 36000; // 10 hrs
    public final static String Srf_PASSWORD_KEY = "SrfPassword";

    private final String srfTunnelPath;
    private final String srfServerName;
    private final String srfProxyName;
    private final String srfAppName;
    private final String srfSecretName;
    private final String srfTestId;
    private String SrfDomain;
    private String SrfProject;
    private String SrfTestSets;
    private String SrfTimeout;
    private String SrfRunMode;
    private String SrfRunHost;

    @DataBoundConstructor
    public RunFromSrfModel(String srfTunnelPath, String srfServerName, String srfProxyName, String srfAppName, String srfSecretName,
                           String srfTestId) {
        this.srfTunnelPath = srfTunnelPath;
        this.srfServerName = srfServerName;
        this.srfProxyName = srfProxyName;
        this.srfTestId = srfTestId;
        this.srfAppName = srfAppName;
        this.srfSecretName = srfSecretName;

        if (!this.SrfTestSets.contains("\n")) {
            this.SrfTestSets += "\n";
        }

        this.SrfTimeout=SrfTimeout;
        this.SrfRunMode = SrfRunMode;

        if (this.SrfRunMode.equals(runModeRemote.getValue())) {
            this.SrfRunHost = SrfRunHost;
        } else {
            this.SrfRunHost = "";
        }

        if (SrfRunHost == null) {
            this.SrfRunHost = "";
        }
    }



    public String getSrfProxyName() {
        return srfProxyName;
    }
    public String getSrfAppName() {
        return srfAppName;
    }
    public String getSrfSecretName() {
        return srfSecretName;
    }

    public String getSrfTestId() {
        return srfTestId;
    }





}
